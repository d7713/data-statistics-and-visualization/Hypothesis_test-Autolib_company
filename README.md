# Autolib Hypothesis test Notebook

### By: Andrew Wairegi

## Description
To create a hypothesis and do a hypothesis test, using an appropriate test statistic.
This will determine whether the null hypothesis is correct or not. It will be then
be documented in a conclusion as to whether the alternative hypothesis was supported
or not.

[Open notebook]

## Setup/installation instructions
1. Find a local folder on your computer
2. Setup the folder as an empty repository (git init)
3. Clone this remote repository into the local repository (git clone)
4. Upload the collaboratory notebook to google drive
5. Open it
6. Upload the dataset file to the file section of the google collab
7. Run the notebook

## Known Bugs
There are no known issues

## Technologies Used
1. Python - programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/data-statistics-and-visualization/Hypothesis_test-Autolib_company/-/blob/main/Autolib_hypothesis_test.ipynb
